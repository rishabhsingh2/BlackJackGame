using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonControl : MonoBehaviour
{
    // Start is called before the first frame update
    public Button GMain;
    public Button BetBtn;
    public Button PickBtn;
    public Button ShowBtn;
    public Button StartNewGame;
    public Button Resume;
    public Button RGame;
    public Button Retry;
    public Text Wallet;
    public Text Details;
    public Text Bet;
    public Text Status;
    public Text Test;
    public Text PlayerTotal;
    public Text DealerTotal;
    public Text GameOverDetails;
    public GameObject MM;
    public GameObject GM;
    public GameObject SS;
    public GameObject SB;
    public GameObject GameOver;
    public Sprite[] CardFace;
    public Image Pcard1;
    public Image Pcard2;
    public Image Dcard1;
    public Image Dcard2;
    public Image SpareCardImage;
    public Image SpareCardAtShow1;
    public Image SpareCardAtShow2;
    public Image SpareCardAtShow3;
    public Image SpareCardAtShow4;

    public bool[] visit=new bool[54];
    public int sgamewon=0;
    public int sgameplayed=0;
    public int samountwon=0;
    public int samountlost=0;
    public int count;
    public int pwallet=500;
    public int dwallet=1000;
    public int bet=0;
    public int playersum=0;
    public int dealersum=0;
    int previous=0;


    public int Pval1,Pval2,Dval1,Dval2=0,Scard=0;

    public int ValueOfCard(int a)
    {
        if((a%13)==0)
            a=10;
        a=a%13;
        if(a>=10)
            a=10;
        return a;
    }

    public void WinOrLose(int a)
    {
        if(playersum>21)
            {
                ShowWinner(1);
            }
        else if(playersum==21)
            {
                ShowWinner(0);
            }
        else if(a!=0)
        {
            if(a==1)
            {
                int temp=playersum+11;
                if(temp>21)
                    a=1;
                else
                    a=11;
            }
            playersum=playersum+a;
            if(playersum>21)
                {
                    ShowWinner(1);
                }
            else if(playersum==21)
                {
                    ShowWinner(0);
                }
        }
        PlayerTotal.text="Player Total : "+playersum;
        DealerTotal.text="Dealer Total : "+dealersum;
    }

    public void ShowWinner(int i)
    {
        PickBtn.gameObject.SetActive(false);
        BetBtn.gameObject.SetActive(false);
        ShowBtn.gameObject.SetActive(false);
        sgameplayed++;
        PlayerPrefs.SetInt("TotalMatch",PlayerPrefs.GetInt("TotalMatch")+1);
        if(i==0)
        {
            sgamewon++;
            samountwon=samountwon+bet;
            PlayerPrefs.SetInt("TotalWin",PlayerPrefs.GetInt("TotalWin")+1);
            PlayerPrefs.SetInt("TotalWA",PlayerPrefs.GetInt("TotalWA")+bet);
            pwallet=pwallet+bet;
            dwallet=dwallet-bet;
            UpdateWallet();
            Status.text="Player Win!!!";
        }
        else
        {
            samountlost=samountlost+bet;
            pwallet=pwallet-bet;
            dwallet=dwallet+bet;
            PlayerPrefs.SetInt("TotalLostAmount",PlayerPrefs.GetInt("TotalLostAmount")+bet);
            Status.text="Dealer Win!!!";
            UpdateWallet();   
        }
        PlayerPrefs.Save();
        for(int j=1;j<54;j++)
        {
            visit[j]=false;
        }
        if(pwallet<=0 || dwallet<=0)
            {
                GM.SetActive(false);
                GameOver.SetActive(true);
                GameOverStats();
            }
        Retry.gameObject.SetActive(true);
    }

    public int RandomGenerate()
    {
        Random r = new Random();
        int temp=Random.Range(1,1000)%53;
        while(visit[temp]==true)
        {
            temp=Random.Range(1,1000)%53;
        }
        visit[temp]=true;
        return temp;
    }

    public void BackToMain()
    {
        SS.SetActive(false);
        MM.SetActive(true);
    }

    public void CardValuePrint(int a,int b,int c)
    {
        Pcard1.sprite=CardFace[a];
        Pcard2.sprite=CardFace[b];
        Dcard1.sprite=CardFace[c];
        Dcard2.sprite=CardFace[Dval2];
        SpareCardImage.sprite=CardFace[Scard];
    }
    public void UpdateWallet()
    {
        Wallet.text="Player Balance : "+pwallet
                   +"\nDealer Balance : "+dwallet;
        Bet.text="Bet Placed : "+bet;
    }
    public void PickCard()
    {
        Scard=RandomGenerate();
        SpareCardImage.sprite=CardFace[Scard];
        Scard=ValueOfCard(Scard);
        Debug.Log("You Picked A Card");
        WinOrLose(Scard);
    }

    public void PlaceBet()
    {
        Debug.Log("Bet Placed");
        if(pwallet<=bet)
        {
            Status.text="Not Enough Balance!!!";
            return;
        }
        else if(dwallet<=bet)
        {
            Status.text="Dealer Is Out Of Money";
            return;
        }
        bet=bet+20;
        Bet.text="Bet Placed : "+bet;
    }

    void PrintPrevious(int a,int count)
    {
        
        if((count%4)==1)
            SpareCardAtShow2.sprite=CardFace[a];
        else if((count%4)==2)    
            SpareCardAtShow3.sprite=CardFace[a];
        else if((count%4)==3)
            SpareCardAtShow4.sprite=CardFace[a];
        else if((count%4)==0)
            SpareCardAtShow1.sprite=CardFace[a];
    }
    public void Show()
    {
        Debug.Log("In Showed");
        Dval2=RandomGenerate();
        Dcard2.sprite=CardFace[Dval2];
        Dval2=ValueOfCard(Dval2);
        if(Dval2==1)
        {
            int temp=dealersum+11;
            if(temp>21)
                Dval2=1;
            else
                Dval2=11;
        }
        dealersum=dealersum+Dval2;
        count=1;
        while(dealersum<17)
            {
                PrintPrevious(previous,count);
                Scard=RandomGenerate();
                previous=Scard;
                SpareCardImage.sprite=CardFace[Scard];
                Scard=ValueOfCard(Scard);
                dealersum=dealersum+Scard;
                count++;
            }
        DealerTotal.text="Dealer Total : "+dealersum;
            if(dealersum>21)
                {
                    ShowWinner(0);
                }
            else
            {
                if(dealersum > playersum || dealersum==21)
                    {
                        ShowWinner(1);
                    }
                else
                {
                    ShowWinner(0);
                }
            }
    }

    public void ShowStats()
    {
        Debug.Log("Stats");
        MM.SetActive(false);
        SS.SetActive(true);
        Details.text="Total Game Played : "+PlayerPrefs.GetInt("TotalMatch")+
                     "\nTotal Game Won : "+PlayerPrefs.GetInt("TotalWin")+
                     "\nTotal Amount Lost : "+PlayerPrefs.GetInt("TotalLostAmount")+
                     "\nTotal Amount Won : "+PlayerPrefs.GetInt("TotalWA");

            
    }
    public void Quit()
    {
        Debug.Log("Quit");
        int temp=PlayerPrefs.GetInt("StartGame");
        if(temp==1)
            {
                PlayerPrefs.SetInt("pwallet",pwallet);
                PlayerPrefs.SetInt("dwallet",dwallet);
                PlayerPrefs.SetInt("bet",bet);
                PlayerPrefs.SetInt("pval1",Pval1);
                PlayerPrefs.SetInt("pval2",Pval2);
                PlayerPrefs.SetInt("dval1",Dval1);
                PlayerPrefs.SetInt("sgameplayed",sgameplayed);
                PlayerPrefs.SetInt("sgamewon",sgamewon);
                PlayerPrefs.SetInt("samountlost",samountlost);
                PlayerPrefs.SetInt("samountwon",samountwon);
                PlayerPrefs.Save();
            }
        Application.Quit();
    }
    public void NewGame()
    {
        pwallet=500;
        dwallet=1000;
        MM.SetActive(false);
        GM.SetActive(false);
        SB.SetActive(true);
    }
    public void ResumeGame()
    {
        MM.SetActive(false);
        SB.SetActive(false);
        GM.SetActive(true);
    }
    public void Shuffle()
    {
        Debug.Log("In Shuffle");
        Retry.gameObject.SetActive(false);
        PlayerPrefs.SetInt("StartGame",1);
        PlayerPrefs.Save();
        SB.SetActive(false);
        GM.SetActive(true);
        PickBtn.gameObject.SetActive(true);
        BetBtn.gameObject.SetActive(true);
        ShowBtn.gameObject.SetActive(true);
        Resume.gameObject.SetActive(true);
        Dval2=0;
        Scard=0;
        bet=0;
        Status.text="";
        playersum=0;
        dealersum=0;
        previous=0;
        SpareCardAtShow1.sprite=CardFace[previous];
        SpareCardAtShow2.sprite=CardFace[previous];
        SpareCardAtShow3.sprite=CardFace[previous];
        SpareCardAtShow4.sprite=CardFace[previous];
        Pval1=RandomGenerate();
        Pval2=RandomGenerate();
        Dval1=RandomGenerate();
        CardValuePrint(Pval1,Pval2,Dval1);
        Pval1=ValueOfCard(Pval1);
        Pval2=ValueOfCard(Pval2);
        Dval1=ValueOfCard(Dval1);
        if(Pval1==1)
            Pval1=11;
        else if(Pval2==1)
            Pval2=11;
        playersum=Pval1+Pval2;
        if(Dval1==1)
            Dval1=11;
        dealersum=Dval1;    
        WinOrLose(0);
        UpdateWallet();
    }

    public void StartGame()
    {
        Debug.Log("Start Game");
        MM.SetActive(false);
        SB.SetActive(true);
        GM.SetActive(false);
    }

    public void MainMenu()
    {
        MM.SetActive(true);
        GM.SetActive(false);
        SB.SetActive(false);
        GameOver.SetActive(false);
        RGame.gameObject.SetActive(false);
        if(PlayerPrefs.GetInt("StartGame")==1)
            Resume.gameObject.SetActive(true);
    }
    public void GameOverStats()
    {
        Resume.gameObject.SetActive(false);
        PlayerPrefs.SetInt("StartGame",0);
        PlayerPrefs.Save();
        GameOverDetails.text="Game Played : "+sgameplayed+
                             "\nGame Won : "+sgamewon+
                             "\nAmount Won : "+samountwon+
                             "\nAmount Lost : "+samountlost;
        sgameplayed=0;
        sgamewon=0;
        samountlost=0;
        samountwon=0;
    }
    public void ReGame()
    {
        Debug.Log("ReGame Pressed");
        Retry.gameObject.SetActive(false);
        MM.SetActive(false);
        GM.SetActive(true);
        SS.SetActive(false);
        SB.SetActive(false);
        pwallet=PlayerPrefs.GetInt("pwallet");
        dwallet=PlayerPrefs.GetInt("dwallet");
        bet=PlayerPrefs.GetInt("bet");
        Dval1=PlayerPrefs.GetInt("dval1");
        Pval1=PlayerPrefs.GetInt("pval1");
        Pval2=PlayerPrefs.GetInt("pval2");
        visit[Dval1]=true;
        visit[Pval1]=true;
        visit[Pval2]=true;
        sgameplayed=PlayerPrefs.GetInt("sgameplayed");
        sgamewon=PlayerPrefs.GetInt("sgamewon");
        samountlost=PlayerPrefs.GetInt("samountlost");
        samountwon=PlayerPrefs.GetInt("samountwon");

        playersum=0;
        dealersum=0;
        Dval2=0;
        Scard=0;
        SpareCardAtShow1.sprite=CardFace[0];
        SpareCardAtShow2.sprite=CardFace[0];
        SpareCardAtShow3.sprite=CardFace[0];
        SpareCardAtShow4.sprite=CardFace[0];
        CardValuePrint(Pval1,Pval2,Dval1);
        Pval1=ValueOfCard(Pval1);
        Pval2=ValueOfCard(Pval2);
        Dval1=ValueOfCard(Dval1);
        if(Pval1==1)
            Pval1=11;
        else if(Pval2==1)
            Pval2=11;
        playersum=Pval1+Pval2;
        if(Dval1==1)
            Dval1=11;
        dealersum=Dval1;    
        WinOrLose(0);
        UpdateWallet();
    }
    void OnEnable()
    {
        Debug.Log("At Start");
        for(int i=0;i<54;i++)
        {
            visit[i]=false;
        }
        visit[0]=true;
        Resume.gameObject.SetActive(false);
        RGame.gameObject.SetActive(false);
        int temp=PlayerPrefs.GetInt("StartGame");
        if(temp==1)
            {
                RGame.gameObject.SetActive(true);
            }
    }
}
